import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';

import authReducer from './authReducer';
import campaignReducer from './campaignReducer';
import viewReducer from './viewReducer';

export default combineReducers({
  auth: authReducer,
  campaign: campaignReducer,
  view: viewReducer,
  form: reduxForm
});
