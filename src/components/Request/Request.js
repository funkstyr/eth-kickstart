import React, { Component } from 'react';

import NewRequest from './components/NewRequest';
import ViewRequest from './components/ViewRequest';

class Request extends Component {
  state = {
    isNew: false,
    index: 0
  };

  async componentDidMount() {
    const { index } = this.props.match.params;
    const isNew = index.includes('new');

    this.setState({ isNew, index: parseInt(index, 10) });
  }

  render() {
    return this.state.isNew ? (
      <NewRequest />
    ) : (
      <ViewRequest index={this.state.index} />
    );
  }
}

export default Request;
