import Loadable from 'react-loadable';
import Loading from './components/Loading';

export const NoMatch = Loadable({
  loader: () => import('./components/NoMatch'),
  loading: Loading
});

export const Index = Loadable({
  loader: () => import('../Index/Index'),
  loading: Loading
});

export const Campaign = Loadable({
  loader: () => import('../Campaign/Campaign'),
  loading: Loading
});
