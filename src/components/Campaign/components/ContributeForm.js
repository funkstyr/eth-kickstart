import React, { Component } from 'react';
import { Form, Input, Message, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

class ContributeForm extends Component {
  state = {
    value: ''
  };

  onSubmit = async event => {
    event.preventDefault();

    this.props
      .addContribution(this.state.value || this.props.min, this.props.contract)
      .then(console.log('update?'));

    console.log('Props', this.props.loading);

    this.setState({ value: '' });
  };
  render() {
    return (
      <Form onSubmit={this.onSubmit} error={!!this.props.error}>
        <Form.Field>
          <label>Contribution Amount</label>
          <Input
            label="ether"
            labelPosition="right"
            value={this.state.value}
            placeholder={this.props.min}
            onChange={e => this.setState({ value: e.target.value })}
          />
        </Form.Field>

        <Button primary content="Contribute" loading={this.props.loading} />

        <Message error header="Error" content={this.props.error} />
      </Form>
    );
  }
}

const mapStateToProps = (state = {}) => {
  return {
    loading: state.view.loadingContribution,
    contract: state.campaign.contract,
    error: state.view.error,
    min: state.campaign.campaign.minContribution
  };
};

export default connect(mapStateToProps, actions)(ContributeForm);
