import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Card, Grid, Button, Table } from 'semantic-ui-react';

import * as actions from '../../actions';
import RequestRow from './components/RequestRow';
import ContributeForm from './components/ContributeForm';
import RequestForm from './components/RequestForm';

class ViewCampaign extends Component {
  componentDidMount() {
    const { address } = this.props.match.params;

    if (address.length > 0) this.props.fetchCampaign(address);

    this.setState({ address });
  }

  renderCards() {
    const {
      balance,
      minContribution,
      patronCount,
      manager
    } = this.props.campaign;

    const items = [
      {
        meta: "Manager's Address",
        description: manager,
        style: { overflowWrap: 'break-word' },
        key: 'manager_card'
      },
      {
        header: minContribution,
        meta: 'Minimum Contribution',
        style: { overflowWrap: 'break-word' },
        key: 'contr_card'
      },
      {
        header: balance,
        meta: 'Balance',
        style: { overflowWrap: 'break-word' },
        key: 'balance_card'
      },
      {
        header: patronCount,
        meta: 'Patron Count',
        style: { overflowWrap: 'break-word' },
        key: 'patron_card'
      }
    ];

    return <Card.Group items={items} />;
  }

  renderRequests() {
    const requests = this.props.campaign.requests || [];

    return requests.map((request, index) => {
      return (
        <RequestRow request={request} key={`request_${index}`} id={index} />
      );
    });
  }

  render() {
    const { Header, Row, HeaderCell, Body } = Table;

    return (
      <div>
        <Helmet title="Campaign" />
        <h3>Campaign</h3>
        <Grid>
          <Grid.Row>
            <Grid.Column width={12}>{this.renderCards()} </Grid.Column>
            <Grid.Column width={4}>
              <ContributeForm address={this.props.address} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              {this.props.isManager ? (
                this.props.requestView ? (
                  <RequestForm />
                ) : (
                  <Button
                    primary
                    content="Add Request"
                    onClick={() => this.props.viewRequest()}
                  />
                )
              ) : null}
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <h4>Expense Requests</h4>

        <Table>
          <Header>
            <Row>
              <HeaderCell>ID</HeaderCell>
              <HeaderCell>Description</HeaderCell>
              <HeaderCell>Amount</HeaderCell>
              <HeaderCell>Recipient</HeaderCell>
              <HeaderCell>Approvals</HeaderCell>
              {this.props.isManager ? (
                <HeaderCell>Complete</HeaderCell>
              ) : (
                <HeaderCell>Approve</HeaderCell>
              )}
            </Row>
          </Header>
          <Body>{this.renderRequests()}</Body>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state = {}) => {
  const { loadingRequest, requestView } = state.view;
  const { manager, account } = state.campaign.campaign;

  return {
    campaign: state.campaign.campaign,
    isManager: account === manager,
    loadingRequest,
    requestView
  };
};

export default connect(mapStateToProps, actions)(ViewCampaign);
