[Demo](https://funkstyr.gitlab.io/eth-kickstart/)

## Uses MetaMask

[Metamask](https://metamask.io/)

### Uses Rinkeby Test Network

[faucet](https://faucet.rinkeby.io/)

* `fox button, top right`: (Click) open metamask
* `top left`: (Click) Netowork to Rinkeby
* `...` : (Click) Copy address to clipboard
* Make `public` post on Twitter, Facebook, or G+
* Copy url to that specific post
* Visit faucet
* Paste link
* `Give me Ether`: (Click) amount of needed ether

```
node: 8.7.x
yarn: 1.3.x
```

### Download Local Copy

```
git clone https://gitlab.com/funkstyr/eth-kickstart.git
cd eth-vending
```

### Install Dependencies

`yarn`

### Run Local Dev

`yarn start`

### Test Contract

`yarn test:contract`

#### Todo:

* refactor contract
* check if manager/patron to show/disable buttons
* ^ stylize for complete/ready requests
* find efficient way of updating campaign info after contract methods
* check for valid address
* look into batching requests w/ web3
